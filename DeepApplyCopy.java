import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * Copies objects from source to destination object, ignoring values that are not set
 * in the source object (null, empty string).
 *
 * Collections and maps will insert values present in the source but not the destination.
 *
 * Values in source object with classes that are in ourPackages will be recursively copied
 * by the same method.
 *
 */
public class DeepApplyCopy {

	private final static Set<String> ourPackages = new HashSet<String>() {{
			//add("org.warb.dto");
	}};

	public DeepApplyCopy addOurPackage(String prefix) {
		ourPackages.add(prefix);
		return this;
	}

	public void applyProperties(Object source, Object target, String... ignoreProperties) {
		Set<String> ignoredPropertySet = new HashSet<String>(Arrays.asList(ignoreProperties));
		Set<PropertyDescriptor> enhancedCopySet = new HashSet<>();
		BeanWrapper src = new BeanWrapperImpl(source);
		BeanWrapper dst = new BeanWrapperImpl(target);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		for(PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (isEmptyValue(srcValue)) {
				ignoredPropertySet.add(pd.getName());
			} else {
				if(Collection.class.isAssignableFrom(pd.getPropertyType()) ||
					isOurPackage(pd.getPropertyType()) ||
					Map.class.isAssignableFrom(pd.getPropertyType())) {
					ignoredPropertySet.add(pd.getName());
					enhancedCopySet.add(pd);
				}
			}
		}

		BeanUtils.copyProperties(source, target,
			ignoredPropertySet.toArray(new String[ignoredPropertySet.size()]));

		for(PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if(!enhancedCopySet.contains(pd)) {continue;}
			Object dstValue = dst.getPropertyValue(pd.getName());
			dstValue = createOrReturn(dstValue, pd.getPropertyType());
			dst.setPropertyValue(pd.getName(), dstValue);
			if(isOurPackage(pd.getPropertyType())) {
				applyProperties(srcValue, dstValue, ignoreProperties);
			} else if(Collection.class.isAssignableFrom(pd.getPropertyType())) {
				applyCollection((Collection)srcValue, (Collection)dstValue, ignoreProperties);
			} else if(Map.class.isAssignableFrom(pd.getPropertyType())) {
				applyMap((Map)srcValue, (Map)dstValue, ignoreProperties);
			}
		}
	}

	private <T> T createOrReturn(T o, Class clazz) {
		if(o != null) {
			return o;
		}

		try {
			T bean = (T)clazz.getConstructor().newInstance();
			return bean;
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException("Could not initialize zero-arg constructor for "+clazz.getCanonicalName(), e);
		}
	}

	private boolean isEmptyValue(Object o) {
		return o == null || (o instanceof String && StringUtils.isEmpty((String)o));
	}

	private boolean isOurPackage(Class c) {
		for(String s : ourPackages) {
			if(c.getCanonicalName().startsWith(s)) {
				return true;
			}
		}
		return false;
	}

	private void applyCollection(Collection src, Collection dst, String... ignoreProperties) {
		for(Object o : src) {
			if(!isEmptyValue(o)) {
				dst.remove(o);
				dst.add(o);
			}
		}
	}

	private void applyMap(Map src, Map dst, String... ignoreProperties) {
		for(Object k : src.keySet()) {
			if(isEmptyValue(src.get(k))) {
				continue;
			}
			if(!dst.keySet().contains(k)) {
				dst.put(k, src.get(k));
			} else {
				if(isOurPackage(src.get(k).getClass())) {
					applyProperties(dst.get(k), src.get(k), ignoreProperties);
				} else {
					dst.remove(k);
					dst.put(k, src.get(k));
				}
			}
		}
	}

}

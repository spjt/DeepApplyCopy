# DeepApplyCopy
Copies objects from source to destination object, ignoring values that are not set
in the source object (null, empty string).

Collections and maps will insert values present in the source but not the destination.

Values in source object with classes that are in ourPackages will be recursively copied
by the same method.
